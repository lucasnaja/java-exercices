package prova;

import javax.swing.JOptionPane;

public class Prova {

	public static void main(String[] args) {
		String nome = "";
		double sal_bruto = 0, imp_renda = 0, plan_saude = 0, sal_familia = 0,
		sal_liquido = 0;
		int num_dep = 0, op;
		do {
			op = Integer.parseInt(JOptionPane.showInputDialog("M E N U  P R I N C I P A L\n1 - Cadastrar funcion�rio\n2 - Calcular imposto de renda\n3 - Calcular plano de sa�de\n4 - Calcular sal�rio fam�lia\n5 - Calcular sal�rio l�quido\n6 - Exibir sal�rio l�quido\n7 - Exibir demais c�lculos\n0 - Encerrar"));
			switch (op) {
				case 1:
					nome = JOptionPane.showInputDialog("Digite o nome do funcion�rio");
					sal_bruto = Double.parseDouble(JOptionPane.showInputDialog("Digite o valor do sal�rio bruto"));
					num_dep = Integer.parseInt(JOptionPane.showInputDialog("Digite o n�mero de dependentes"));
					break;
				case 2:
					if (sal_bruto < 1100) imp_renda = 0;
					else if (sal_bruto < 2000) imp_renda = sal_bruto * 0.15;
					else imp_renda = sal_bruto * 0.25;
					
					JOptionPane.showMessageDialog(null, "Imposto de renda: R$" + imp_renda);
					break;
				case 3:
					if (sal_bruto < 500) plan_saude = 0;
					else if (sal_bruto < 1000)	plan_saude = sal_bruto * 0.01;
					else if (sal_bruto < 2000)	plan_saude = sal_bruto * 0.03;
					else plan_saude = sal_bruto * 0.05;
					
					JOptionPane.showMessageDialog(null, "Plano de sa�de: R$" + plan_saude);
					break;
				case 4:
					if (sal_bruto < 549.81)	sal_familia = num_dep * 16.53;
					else sal_familia = num_dep * 8.83;
					
					JOptionPane.showMessageDialog(null, "Sal�rio fam�lia: R$" + sal_familia);
					break;
				case 5:
					sal_liquido = sal_bruto - plan_saude - imp_renda + sal_familia;
					
					JOptionPane.showMessageDialog(null, "Sal�rio l�quido: R$" + sal_liquido);
					break;
				case 6:
					JOptionPane.showMessageDialog(null, "Nome do funcion�rio: " + nome + "\nN�mero de dependentes: " + num_dep + "\nSal�rio bruto: R$" + sal_bruto + "\nPlano de sa�de: R$" + plan_saude + "\nImposto de renda: R$" + imp_renda + "\nSal�rio fam�lia: R$" + sal_familia + "\nSal�rio l�quido: R$" + sal_liquido);
					break;
				case 7:
					int op2;
					do {
						op2 = Integer.parseInt(JOptionPane.showInputDialog("M E N U  E X I B I � � O\n1 - Exibir imposto de renda\n2 - Exibir plano de sa�de\n3 - Exibir sal�rio fam�lia\n0 - Voltar"));
						switch (op2) {
							case 1:
								JOptionPane.showMessageDialog(null, "Imposto de renda: R$" + imp_renda);
								break;
							case 2:
								JOptionPane.showMessageDialog(null, "Plano de sa�de: R$" + plan_saude);
								break;
							case 3:
								JOptionPane.showMessageDialog(null, "Sal�rio fam�lia: R$" + sal_liquido);
								break;
						}
					} while (op2 != 0);
					break;
				case 0: break;
				default:
					JOptionPane.showMessageDialog(null, "Op��o inv�lida.");
			}
		} while (op != 0);
	}
}