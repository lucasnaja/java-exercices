package pacote_Lucas;

import javax.swing.JOptionPane;

public class class_Lucas {
	public static void main(String[] args) {
		int op = 0;
		do {
			op = Integer.parseInt(JOptionPane.showInputDialog("1 - Do-While\n2 - For\n3 - IF\n4 - Switch-Case\n0 - Sair"));
			switch (op) {
				case 1:
					int num = Integer.parseInt(JOptionPane.showInputDialog("Menu principal: Digite um n�mero"));
					do {
						switch (num) {
							case 1:
								int num2 = Integer.parseInt(JOptionPane.showInputDialog("In�cio: Digite um n�mero:"));
								switch (num2) {
									case 1: JOptionPane.showMessageDialog(null, "Recortar"); break;
									case 2: JOptionPane.showMessageDialog(null, "Copiar"); break;
									case 3: JOptionPane.showMessageDialog(null, "Colar"); break;
									case 4: JOptionPane.showMessageDialog(null, "Formatar"); break;
									case 0:
										num = Integer.parseInt(JOptionPane.showInputDialog("Menu principal: Digite um n�mero"));
								}
								break;
							case 2:
								int num3 = Integer.parseInt(JOptionPane.showInputDialog("Inserir: Digite um n�mero:"));
								switch (num3) {
									case 1: JOptionPane.showMessageDialog(null, "Tabela"); break;
									case 2:
										int num5;
										do {
											num5 = Integer.parseInt(JOptionPane.showInputDialog("Imagem: Digite um n�mero:"));
											switch (num5) {
												case 1: JOptionPane.showMessageDialog(null, "Desktop"); break;
												case 2: JOptionPane.showMessageDialog(null, "Meus Documentos"); break;
												case 3: JOptionPane.showMessageDialog(null, "Meu Computador"); break;
												case 4: JOptionPane.showMessageDialog(null, "Meus Locais de Rede"); break;
											}
										} while (num5 != 0); break;							
									case 3: JOptionPane.showMessageDialog(null, "Clip"); break;
									case 0:
										num = Integer.parseInt(JOptionPane.showInputDialog("Menu principal: Digite um n�mero"));
								}
								break;
							case 3:
								int num4 = Integer.parseInt(JOptionPane.showInputDialog("Layout: Digite um n�mero:"));
								switch (num4) {
									case 1: JOptionPane.showMessageDialog(null, "Margem"); break;
									case 2: JOptionPane.showMessageDialog(null, "Colunas"); break;
									case 3: JOptionPane.showMessageDialog(null, "Quebras"); break;
									case 4: JOptionPane.showMessageDialog(null, "Hifeniza��o"); break;
									case 0:
										num = Integer.parseInt(JOptionPane.showInputDialog("Menu principal: Digite um n�mero"));
								}
								break;
						}
					} while (num != 0);
				break;
				case 2:
					int[] numFor = new int[10];
					String cresc = "", decresc = "";
					
					for (int i = 0; i < numFor.length; i++) {
						numFor[i] = Integer.parseInt(JOptionPane.showInputDialog("Digite o " + (i + 1) + "� n�mero: "));
					}
						
					for (int i = 0; i < numFor.length; i++)
						for (int j = i + 1; j < numFor.length; j++)
							if (numFor[i] > numFor[j]) {
								int temp = numFor[i];
								numFor[i] = numFor[j];
								numFor[j] = temp;
							}
						
					
					for (int i = 1; i <= numFor.length; i++) {
						cresc += i == numFor.length ? numFor[i - 1] + "." : numFor[i - 1] + ", ";
						decresc += i == numFor.length ? numFor[numFor.length - i] + "." : numFor[numFor.length - i] + ", ";
					}
					
					JOptionPane.showMessageDialog(null, "Ordem crescente: " + cresc + '\n' +
														"Ordem decrescente: " + decresc);
					break;
				case 3:
					int idade;
					
					idade = Integer.parseInt(JOptionPane.showInputDialog("Digite a idade do atleta:"));
					
					if (idade < 5)
						JOptionPane.showMessageDialog(null, "N�o permitido");
					else if (idade < 8)
						JOptionPane.showMessageDialog(null, "Infantil A");
					else if (idade < 12)
						JOptionPane.showMessageDialog(null, "Infantil B");
					else if (idade < 14)
						JOptionPane.showMessageDialog(null, "Juvenil A");
					else if (idade < 18)
						JOptionPane.showMessageDialog(null, "Juvenil B");
					else JOptionPane.showMessageDialog(null, "Adultos");
					break;
				case 4:
					String nome = "";
					double sal_bruto = 0, imp_renda = 0, plan_saude = 0, sal_familia = 0,
					sal_liquido = 0;
					int num_dep = 0, opSwitch;
					do {
						opSwitch = Integer.parseInt(JOptionPane.showInputDialog("M E N U  P R I N C I P A L\n1 - Cadastrar funcion�rio\n2 - Calcular imposto de renda\n3 - Calcular plano de sa�de\n4 - Calcular sal�rio fam�lia\n5 - Calcular sal�rio l�quido\n6 - Exibir sal�rio l�quido\n7 - Exibir demais c�lculos\n0 - Encerrar"));
						switch (opSwitch) {
							case 1:
								nome = JOptionPane.showInputDialog("Digite o nome do funcion�rio");
								sal_bruto = Double.parseDouble(JOptionPane.showInputDialog("Digite o valor do sal�rio bruto"));
								num_dep = Integer.parseInt(JOptionPane.showInputDialog("Digite o n�mero de dependentes"));
								break;
							case 2:
								if (sal_bruto < 1100) imp_renda = 0;
								else if (sal_bruto < 2000) imp_renda = sal_bruto * 0.15;
								else imp_renda = sal_bruto * 0.25;
								
								JOptionPane.showMessageDialog(null, "Imposto de renda: R$" + imp_renda);
								break;
							case 3:
								if (sal_bruto < 500) plan_saude = 0;
								else if (sal_bruto < 1000)	plan_saude = sal_bruto * 0.01;
								else if (sal_bruto < 2000)	plan_saude = sal_bruto * 0.03;
								else plan_saude = sal_bruto * 0.05;
								
								JOptionPane.showMessageDialog(null, "Plano de sa�de: R$" + plan_saude);
								break;
							case 4:
								if (sal_bruto < 549.81)	sal_familia = num_dep * 16.53;
								else sal_familia = num_dep * 8.83;
								
								JOptionPane.showMessageDialog(null, "Sal�rio fam�lia: R$" + sal_familia);
								break;
							case 5:
								sal_liquido = sal_bruto - plan_saude - imp_renda + sal_familia;
								
								JOptionPane.showMessageDialog(null, "Sal�rio l�quido: R$" + sal_liquido);
								break;
							case 6:
								JOptionPane.showMessageDialog(null, "Nome do funcion�rio: " + nome + "\nN�mero de dependentes: " + num_dep + "\nSal�rio bruto: R$" + sal_bruto + "\nPlano de sa�de: R$" + plan_saude + "\nImposto de renda: R$" + imp_renda + "\nSal�rio fam�lia: R$" + sal_familia + "\nSal�rio l�quido: R$" + sal_liquido);
								break;
							case 7:
								int op2;
								do {
									op2 = Integer.parseInt(JOptionPane.showInputDialog("M E N U  E X I B I � � O\n1 - Exibir imposto de renda\n2 - Exibir plano de sa�de\n3 - Exibir sal�rio fam�lia\n0 - Voltar"));
									switch (op2) {
										case 1:
											JOptionPane.showMessageDialog(null, "Imposto de renda: R$" + imp_renda);
											break;
										case 2:
											JOptionPane.showMessageDialog(null, "Plano de sa�de: R$" + plan_saude);
											break;
										case 3:
											JOptionPane.showMessageDialog(null, "Sal�rio fam�lia: R$" + sal_liquido);
											break;
									}
								} while (op2 != 0);
								break;
							case 0: break;
							default:
								JOptionPane.showMessageDialog(null, "Op��o inv�lida.");
						}
					} while (opSwitch != 0);
					break;
			}
		} while(op != 0);
	}
}