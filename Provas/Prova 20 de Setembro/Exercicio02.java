package provaPA;

import javax.swing.JOptionPane;

public class Exercicio02 {

	public static void main(String[] args) {
		double valorSalario, valorAumento, valorCorrigido;

		valorSalario = Double.parseDouble(JOptionPane.showInputDialog("Digite seu salário"));

		if (valorSalario <= 700)
			valorAumento = 0.15 * valorSalario;
		else if (valorSalario <= 1800)
			valorAumento = 0.1 * valorSalario;
		else if (valorSalario <= 2500)
			valorAumento = 0.07 * valorSalario;
		else
			valorAumento = 0.05 * valorSalario;
		
		valorCorrigido = valorSalario + valorAumento;
		
		JOptionPane.showMessageDialog(null, "Salário atual: R$" + valorSalario + '\n' +
				"Aumento: R$" + valorAumento + '\n' +
				"Salário corrigido: R$" + valorCorrigido, "Salário", 1);
	}
}