package provaPA;

import javax.swing.JOptionPane;

public class Exercicio01 {

	public static void main(String[] args) {
		int tempoContribuicao, idade, tempoRestante;
		String sexo;
		
		tempoContribuicao = Integer.parseInt(JOptionPane.showInputDialog("Digite o tempo de contribuição"));
		idade = Integer.parseInt(JOptionPane.showInputDialog("Digite sua idade"));
		sexo = JOptionPane.showInputDialog("Digite o seu sexo [M/F]");
		
		if (sexo.equalsIgnoreCase("M")) {
			if (tempoContribuicao + idade >= 95)
				JOptionPane.showMessageDialog(null, "Já pode se aposentar!", "Aposentadoria", 1);
			else {
				tempoRestante = (95 - (tempoContribuicao + idade)) / 2;
				JOptionPane.showMessageDialog(null, "Não pode se aposentar! Tempo restante: " + tempoRestante + " anos", "Aposentadoria", 0);
			}
		} else if (sexo.equalsIgnoreCase("F")) {
			if (tempoContribuicao + idade >= 85)
				JOptionPane.showMessageDialog(null, "Já pode se aposentar!", "Aposentadoria", 1);
			else {
				tempoRestante = (85 - (tempoContribuicao + idade)) / 2;
				JOptionPane.showMessageDialog(null, "Não pode se aposentar! Tempo restante: " + tempoRestante + " anos", "Aposentadoria", 0);
			}
		} else
			JOptionPane.showMessageDialog(null, "Sexo inválido!");
	}
}
