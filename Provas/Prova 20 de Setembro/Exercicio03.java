package provaPA;

import javax.swing.JOptionPane;

public class Exercicio03 {

	public static void main(String[] args) {
		int numFaltasPA, numFaltasBD;
		double freqGeral;
		String notaPA, notaBD;
		
		numFaltasPA = Integer.parseInt(JOptionPane.showInputDialog("Digite as faltas da PA"));
		numFaltasBD = Integer.parseInt(JOptionPane.showInputDialog("Digite as faltas do BD"));
		notaPA = JOptionPane.showInputDialog("Digite a nota da PA");
		notaBD = JOptionPane.showInputDialog("Digite a nota do BD");
		
		freqGeral = (210 - (numFaltasPA + numFaltasBD)) / 2.1;
		
		if (freqGeral < 75d)
			JOptionPane.showMessageDialog(null, "Retido", "Resultado", 0);
		else if (freqGeral >= 75d && (notaPA.equalsIgnoreCase("I") || notaBD.equalsIgnoreCase("I")))
			JOptionPane.showMessageDialog(null, "Retido", "Resultado", 0);
		else
			JOptionPane.showMessageDialog(null, "Aprovado", "Resultado", 1);
	}
}
