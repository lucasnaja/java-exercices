
package exercicios;

import javax.swing.JOptionPane;

public class Exercicios {
    public static void main(String[] args) {
        int M[][] = new int[4][6],
            N[][] = new int[4][6],
            produto[][] = new int[4][6],
            soma[][] = new int[4][6],
            diferenca[][] = new int[4][6];
        
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 6; j++) {
                M[i][j] = (int)(Math.random() * 10);
                System.out.print(M[i][j] + "|");
            }
            System.out.println("");
        }
        
        System.out.println();
        
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 6; j++) {
                N[i][j] = (int)(Math.random() * 10);
                System.out.print(N[i][j] + "|");
            }
            System.out.println();
        }
        
        System.out.println("Produto");
        
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 6; j++) {
                produto[i][j] = M[i][j] * N[i][j];
                soma[i][j] = M[i][j] + N[i][j];
                diferenca[i][j] = M[i][j] - N[i][j];
                System.out.print(produto[i][j] + " - ");
            }
        
        System.out.println("\nSoma");
        
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 6; j++)
                System.out.print(soma[i][j] + " - ");
        
        System.out.println("\nDiferença");
        
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 6; j++)
                System.out.print(diferenca[i][j] + " - ");
        
        System.out.println("\n\n");
        
        int M2[][] = new int[6][6],
            N2[][] = new int[6][6],
            A = Integer.parseInt(JOptionPane.showInputDialog("Digite um número:"));
        
        for (int i = 0; i < 6; i++)
            for (int j = 0; j < 6; j++) {
                M2[i][j] = (int)(Math.random() * 10);
                N2[i][j] = M2[i][j] * A;
                System.out.print(M2[i][j] + " - ");
            }
        
        System.out.println();
        
        for (int i = 0; i < 6; i++)
            for (int j = 0; j < 6; j++)
                System.out.print(N2[i][j] + " - ");
        
        System.out.println("\n\n");
        
        int num1 = Integer.parseInt(JOptionPane.showInputDialog("Digite um número:")),
            num2 = Integer.parseInt(JOptionPane.showInputDialog("Digite um número:")),
            matriz[][] = new int[num1][num2];    
        
        for (int i = 0; i < num1; i++) {
            for (int j = 0; j < num2; j++) {
                matriz[i][j] = (int)(Math.random() * 10);
                System.out.print("Posição: " + i + " x " + j + " = " + matriz[i][j] + '\n');
            }            
            System.out.println();
        }
        
        int num3 = Integer.parseInt(JOptionPane.showInputDialog("Digite um número:")),
            num4 = Integer.parseInt(JOptionPane.showInputDialog("Digite um número:")),
            matriz2[][] = new int[num3][num4];    
        
        for (int i = 0; i < num3; i++) {
            int soma2 = 0;
            for (int j = 0; j < num4; j++) {
                matriz2[i][j] = (int)(Math.random() * 10);
                soma2 += matriz2[i][j];
                System.out.print("\t" + matriz2[i][j] + "|");
            }            
            System.out.println(" = " + soma2);
        }
                
        int matriz3[][] = new int[10][10],
            num5 = Integer.parseInt(JOptionPane.showInputDialog("Digite um número:"));
        
        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++)
                matriz3[i][j] = (int)(Math.random() * 10);
        
        boolean tem = false;
        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++)
                if (matriz3[i][j] == num5) {
                    tem = true;
                    System.out.println("Número encontrado na linha " + i + " coluna " + j);
                } else if (tem == false && i == 9 && j == 9)
                    System.out.println("Número não encontrado!");
    }
}