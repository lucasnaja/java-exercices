package exercicios;

import javax.swing.JOptionPane;

public class Exercicio01 {

	public static void main(String[] args) {
		double diaria = 100;
		double taxaDeServicos = 15;		
		
		int diasHospedados = Integer.parseInt(JOptionPane.showInputDialog("Digite o número de dias hospedados:"));
		
		double totalDasDiarias = diasHospedados * diaria;
		double totalDaTaxaDeServicos = taxaDeServicos * diasHospedados;
		
		double totalPagar = totalDasDiarias + totalDaTaxaDeServicos;
		
		JOptionPane.showMessageDialog(null, "Dias hospedados: " + diasHospedados + '\n' +
		"Total das Diarias: " + totalDasDiarias + '\n' +
		"Total da Taxa de Serviços: " + totalDaTaxaDeServicos + '\n' +
		"Total a pagar: " + totalPagar, "Resultado", 1);
	}

}
