package exercicios;

import javax.swing.JOptionPane;

public class Exercicio03 {

	public static void main(String[] args) {		
		double preco = Double.parseDouble(JOptionPane.showInputDialog("Entre com o pre�o do produto: "));
		double desconto = Double.parseDouble(JOptionPane.showInputDialog("Valor do desconto em %"));	
		
		desconto = desconto/100;
		
		double precoFinal = preco - preco * desconto;
		
		JOptionPane.showMessageDialog(null, "Valor do produto: "  + preco + '\n' +
	    "Valor do desconto: " + preco * desconto + '\n' +
		"Valor final do produto: " + precoFinal, "Resultado", 1);
	}

}
