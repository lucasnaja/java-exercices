package exercicios;

import javax.swing.JOptionPane;

public class Exercicio04 {

	public static void main(String[] args) {		
		int numeroDeEmpregados = Integer.parseInt(JOptionPane.showInputDialog("N�mero de empregados da loja: "));
		
		double salarioMinimo = Double.parseDouble(JOptionPane.showInputDialog("Entre como valor do sal�rio m�nimo: "));
		
		double precoDeCusto = Double.parseDouble(JOptionPane.showInputDialog("Digite o pre�o de custo da bicicleta: "));	
		
		double quantidadeVendida = Double.parseDouble(JOptionPane.showInputDialog("Digite a quantidade de bicicletas vendidas: "));
		
		double totalVendida = quantidadeVendida * precoDeCusto;
		double comissao = (totalVendida * 0.1) / numeroDeEmpregados;
		
		double salarioTotal = salarioMinimo + comissao;
		
		JOptionPane.showMessageDialog(null, "Comiss�o sobre vendas: " + comissao + '\n' +
		"Sal�rio total: " + salarioTotal, "Resultado", 1);
	}

}
