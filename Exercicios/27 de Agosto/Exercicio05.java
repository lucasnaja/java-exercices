package exercicios;

import javax.swing.JOptionPane;

public class Exercicio05 {

	public static void main(String[] args) {		
		double nota1 = Double.parseDouble(JOptionPane.showInputDialog("Digite a 1� nota:"));
		
		double nota2 = Double.parseDouble(JOptionPane.showInputDialog("Digite a 2� nota:"));	
		
		double nota3 = Double.parseDouble(JOptionPane.showInputDialog("Digite a 3� nota:"));		
		
		double nota4 = Double.parseDouble(JOptionPane.showInputDialog("Digite a 4� nota: "));
		
		double faltas = Double.parseDouble(JOptionPane.showInputDialog("Entre com o n�mero de faltas:"));
		
		double aulas = Double.parseDouble(JOptionPane.showInputDialog("Quantidade de aulas dadas: "));
		
		double media = (nota1 + nota2 + nota3 + nota4) / 4;
		
		double frequencia = (aulas - faltas) / aulas * 100;
		faltas = faltas / aulas * 100;
		
		JOptionPane.showMessageDialog(null, "M�dia do aluno: " + media + '\n' +
		"Frequencia: " + frequencia + "%" + '\n' +
		"Faltas: " + faltas + "%", "Resultado", 1);

	}

}
