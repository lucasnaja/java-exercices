package exercicios;

import javax.swing.JOptionPane;

public class Exercicio02 {

	public static void main(String[] args) {			
		double distancia = Double.parseDouble(JOptionPane.showInputDialog("Digite a dist�ncia percorrida em kms:"));
		double litros = Double.parseDouble(JOptionPane.showInputDialog("Combust�vel gasto em litros:"));
		double consumo = distancia / litros; 
		
		JOptionPane.showMessageDialog(null, "O consumo do autom�vel foi de " + consumo + " km/l", "Resultado", 1);
	}

}
