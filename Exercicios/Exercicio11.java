package exercicios;

public class Exercicio11 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		double custoFabrica, porcDist, porcImp;
		
		System.out.print("Digite o custo de fábrica do carro: ");
		custoFabrica = ent.nextDouble();
		
		porcDist = custoFabrica * 0.28;
		porcImp = custoFabrica * 0.45;
		
		System.out.println("O preço final do carro é: R$" + (custoFabrica + porcDist + porcImp));
	}
}