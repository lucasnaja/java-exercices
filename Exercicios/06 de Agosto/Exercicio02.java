package packAula;

public class Exercicio02 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		double qtdChuva, qtdPolegadas;
		
		System.out.print("Digite a quantidade de �gua em polegadas: ");
		qtdPolegadas = ent.nextDouble();
		System.out.println("A chuva em polegadas �: " + qtdPolegadas + "p");
		qtdChuva = qtdPolegadas * 25.4d;
		System.out.println("Chuva em mm: " + qtdChuva + "mm");
	}

}
