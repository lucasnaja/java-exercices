package packAula;

public class Exercicio04 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		int qtdComprada;
		double preco, margemLucro;
		
		System.out.print("Digite quantos produtos o cliente ir� comprar: ");
		qtdComprada = ent.nextInt();
		
		System.out.println("O cliente ir� comprar " + qtdComprada + " produtos.");
		
		System.out.print("Digite o pre�o de seu produto: ");
		preco = ent.nextDouble();
		System.out.println("Seu produto ir� custar: R$" + preco);
		
		System.out.print("Digite a margem de lucro que voc� quer obter: ");
		margemLucro = ent.nextDouble();		
		
		System.out.println("O pre�o final de seu produto com a margem de lucro �: R$" + ((preco * qtdComprada) + (preco * qtdComprada) * (margemLucro / 100.0d)));
		System.out.println("O lucro obtido foi: " + (preco * qtdComprada) * (margemLucro / 100.0d));
	}

}
