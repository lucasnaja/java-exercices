package packAula;

public class Exercicio01 {

	public static void main(String[] args) {
		java.util.Scanner tec = new java.util.Scanner(System.in);
		double largura, larguraCM;
		
		System.out.print("Digite a largura da parede em metros: ");
		
		largura = tec.nextDouble();
		System.out.println("Largura da parede: " + largura + "m");
		
		larguraCM = largura * 100.0d;
		System.out.print("A largura da parede em CM �: " + larguraCM + "cm");
	}

}
