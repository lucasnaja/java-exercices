package exercicios;

public class Exercicio19 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		int n;
		
		System.out.print("Digite o número da tabuada: ");
		n = ent.nextInt();
		
		for (int i = 1; i <= 10; i++)
			System.out.println(i + " x " + n + " = " + i * n);
	}
}