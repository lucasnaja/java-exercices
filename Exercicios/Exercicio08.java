package exercicios;

public class Exercicio08 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		int base, altura;
		double area;
		
		System.out.print("Digite a base do triângulo (em metros): ");
		base = ent.nextInt();
		System.out.print("Digite a altura do triângulo (em metros): ");
		altura = ent.nextInt();
		
		area = (base * altura) / 2d;
		System.out.println("A área do triângulo é: " + area + "m²");
	}
}