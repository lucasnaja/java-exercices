package exercicios;

public class Exercicio05 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		int n, qtdDiv = 0;
		
		System.out.print("Digite um número: ");
		n = ent.nextInt();
		for (int i = n; i > 0; i--)
			if (n % i == 0)
				qtdDiv++;
		
		if (qtdDiv == 2)
			System.out.println(n + " é um número primo!");
		else System.out.println(n + " não é um número primo!");
	}
}