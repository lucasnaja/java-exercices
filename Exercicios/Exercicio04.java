package exercicios;

public class Exercicio04 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		int numeros[] = new int[20], soma = 0;
		
		for (int i = 0; i < numeros.length; i++) {
			System.out.print("Digite o " + (i + 1) + "° número: ");
			numeros[i] = ent.nextInt();
			if (numeros[i] % 2 == 0)
				soma += numeros[i];
		}
		
		System.out.println("A soma dos números pares é: " + soma);
	}
}