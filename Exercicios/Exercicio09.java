package exercicios;

public class Exercicio09 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		int numEleitores, votosBrancos, votosNulos, votosValidos;
		double porcBrancos, porcNulos, porcValidos;
		
		System.out.print("Digite o total de eleitores: ");
		numEleitores = ent.nextInt();
		System.out.print("Digite o total de votos brancos: ");
		votosBrancos = ent.nextInt();
		System.out.print("Digite o total de votos nulos: ");
		votosNulos = ent.nextInt();
		System.out.print("Digite o total de votos válidos: ");
		votosValidos = ent.nextInt();
		
		porcBrancos = ((double)votosBrancos / numEleitores) * 100;
		porcNulos = ((double)votosNulos / numEleitores) * 100;
		porcValidos = ((double)votosValidos / numEleitores) * 100;
		
		
		System.out.println("Porcentagem de votos Brancos: " + porcBrancos + '%');
		System.out.println("Porcentagem de votos Nulos: " + porcNulos + '%');
		System.out.println("Porcentagem de votos Válidos: " + porcValidos + '%');
	}
}