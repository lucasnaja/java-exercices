package exercicios;

public class Exercicio20 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		int n;
		double media = 0;
		
		System.out.print("Digite a quantidade de números: ");
		n = ent.nextInt();
		int[] numeros = new int[n];
		
		for (int i = 0; i < numeros.length; i++) {
			System.out.print("Digite o " + (i + 1) + "° número: ");
			numeros[i] = ent.nextInt();
			media += (double)numeros[i] / numeros.length;
		}
		
		System.out.println("A média de todos os números foi: " + media);
	}
}