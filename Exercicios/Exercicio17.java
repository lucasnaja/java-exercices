package exercicios;

public class Exercicio17 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		int valor1 = 0, valor2 = 0;
		
		System.out.print("Digite o primeiro valor: ");
		valor1 = ent.nextInt();
		do {
			System.out.print("Digite o segundo valor: ");
			valor2 = ent.nextInt();
		} while(valor2 == valor1);
		
		System.out.println("Maior valor: " + (valor1 < valor2 ? valor2 : valor1));
	}
}