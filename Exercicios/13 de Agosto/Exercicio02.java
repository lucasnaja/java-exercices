package exercicios;

public class Exercicio02 {
	public static void main(String args[]) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		double num1, num2, num3, num4, soma;
		
		System.out.print("Digite o primeiro n�mero: ");
		num1 = ent.nextDouble();
		
		System.out.print("Digite o segundo n�mero: ");
		num2 = ent.nextDouble();
		
		System.out.print("Digite o terceiro n�mero: ");
		num3 = ent.nextDouble();
		
		System.out.print("Digite o quarto n�mero: ");
		num4 = ent.nextDouble();
		
		soma = (num1 * num1) + (num2 * num2) + (num3 * num3) + (num4 * num4);
		System.out.println("O quadrado dos 4 n�meros s�o: " + soma);
	}
}
