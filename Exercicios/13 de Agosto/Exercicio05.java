package exercicios;

public class Exercicio05 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		double sal_minimo, horas_trabalhadas, horas_extras, sal_bruto, ganho_horas,
		ganho_dep, hora_extra_trabalhada, imposto_renda, grat, sal_liquido;
		int num_dep;
		
		System.out.print("Digite o sal�rio m�nimo: ");
		sal_minimo = ent.nextDouble();
		
		System.out.print("Digite o n�mero de horas trabalhadas: ");
		horas_trabalhadas = ent.nextDouble();
		
		System.out.print("Digite o n�mero de dependentes: ");
		num_dep = ent.nextInt();
		
		System.out.print("Digite o n�mero de horas extras trabalhadas: ");
		horas_extras = ent.nextDouble();
		
		ganho_horas = 1.0/5.0 * sal_minimo;
		ganho_dep = num_dep * 32.0;
		hora_extra_trabalhada = (ganho_horas + ganho_horas * 50.0/100.0);
		sal_bruto = ganho_dep + hora_extra_trabalhada + horas_trabalhadas * ganho_horas;
		imposto_renda = 22.5/100.0 * sal_bruto;
		grat = 5.5/100.0 * sal_bruto;
		sal_liquido = sal_bruto - imposto_renda + grat;
		
		System.out.println("Ganho por horas trabalhadas: " + ganho_horas);
		System.out.println("Ganho por dependentes: " + ganho_dep);
		System.out.println("Ganho por hora extra: " + hora_extra_trabalhada);
		System.out.println("Sal�rio bruto: " + sal_bruto);
		System.out.println("Imposto de renda: " + imposto_renda);
		System.out.println("Gratifica��o: " + grat);
		System.out.println("Sal�rio l�quido: " + sal_liquido);
	}

}
