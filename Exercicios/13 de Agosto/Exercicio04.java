package exercicios;

public class Exercicio04 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		String nome;
		double horas_trabalhadas, valor_hora;
		
		System.out.print("Digite o nome do funcion�rio: ");
		nome = ent.nextLine();
		
		System.out.print("Digite o n�mero de horas trabalhadas: ");
		horas_trabalhadas = ent.nextDouble();
		
		System.out.print("Digite o valor que ele recebe por hora: ");
		valor_hora = ent.nextDouble();
		
		System.out.println("Nome do funcion�rio: " + nome);
		System.out.println("Valor que ele recebeu: " + horas_trabalhadas * valor_hora);
	}

}
