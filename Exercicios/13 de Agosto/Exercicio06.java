package exercicios;

public class Exercicio06 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		double valor_prod, perc_desconto;
		
		System.out.print("Digite o valor do produto: ");
		valor_prod = ent.nextDouble();
		
		System.out.print("Digite o percentual de desconto: ");
		perc_desconto = ent.nextDouble();
		
		System.out.println("O valor final do produto �: " + (valor_prod - perc_desconto/100 * valor_prod));
	}

}
