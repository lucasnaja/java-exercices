package exercicios;

public class Exercicio05 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		double nota1, nota2, nota3, nota4, presencaPorcentagem, faltasPorcentagem;
		int aulasDadas, faltas;
		
		
		System.out.print("Digite a primeira nota do aluno: ");
		nota1 = ent.nextDouble();

		System.out.print("Digite a segunda nota do aluno: ");
		nota2 = ent.nextDouble();
		
		System.out.print("Digite a terceira nota do aluno: ");
		nota3 = ent.nextDouble();
		
		System.out.print("Digite a quarta nota do aluno: ");
		nota4 = ent.nextDouble();
		
		System.out.print("Digite a quantidade de faltas: ");
		faltas = ent.nextInt();
		
		System.out.print("Digite a quantidade de aulas dadas: ");
		aulasDadas = ent.nextInt();
		
		System.out.println("A m�dia de notas foi: " + (nota1 + nota2 + nota3 + nota4) / 4.0d);
		
		presencaPorcentagem = (double)(aulasDadas - faltas) / (double)aulasDadas * 100.0d;
		System.out.println("A frequ�ncia foi: " + presencaPorcentagem);
		
		faltasPorcentagem = (double)faltas / (double)aulasDadas * 100.0d;
		System.out.println("As faltas foram: " + faltasPorcentagem);
	}

}
