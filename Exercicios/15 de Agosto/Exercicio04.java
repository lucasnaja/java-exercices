package exercicios;

public class Exercicio04 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		int totalEmpregados, bicicletasVendidas;
		double sal_minimo, precoBicicleta;
		
		System.out.print("Digite o n�mero de empregados: ");
		totalEmpregados = ent.nextInt();
		
		System.out.print("Digite o valor do sal�rio m�nimo: ");
		sal_minimo = ent.nextDouble();
		
		System.out.print("Digite o pre�o de custo da bicicleta: ");
		precoBicicleta = ent.nextDouble();
		
		System.out.print("Digite o total de bicicletas vendidas: ");
		bicicletasVendidas = ent.nextInt();
		
		double salarioTotal = sal_minimo + (bicicletasVendidas * precoBicicleta * 10/100 / totalEmpregados);
		double lucro_liquido = (precoBicicleta + precoBicicleta * 50/100.0d) * bicicletasVendidas;
		
		System.out.println("Sal�rio total do funcion�rio: " + salarioTotal);
		System.out.println("Lucro l�quido da loja: " + lucro_liquido);
	}

}
