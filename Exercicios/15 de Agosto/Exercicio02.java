package exercicios;

public class Exercicio02 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		double distancia, combustivel;
		
		System.out.print("Digite a dist�ncia total percorrida pelo carro, em km: ");
		distancia = ent.nextDouble();
		
		System.out.print("Digite o combust�vel gasto, em litros: ");
		combustivel = ent.nextDouble();
		
		System.out.println("A m�ida gasta de combust�vel, em litros, foi: " + distancia / combustivel + " litros/km");
	}

}
