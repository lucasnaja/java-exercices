package exercicios;

public class Exercicio03 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		double preco, valorDesconto, descontoPorcentagem;
		
		System.out.print("Digite o pre�o do produto: ");
		preco = ent.nextDouble();
		
		System.out.print("Digite o pre�o do desconto: ");
		valorDesconto = ent.nextDouble();
		
		descontoPorcentagem = valorDesconto / preco * 100.0d;
		
		System.out.println("O desconto em porcentagem � igual a " + descontoPorcentagem + "%");
		System.out.println("O pre�o final do produto �: " + (preco - valorDesconto));
	}

}
