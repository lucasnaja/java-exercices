package exercicios;

public class Exercicio10 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		double salAtual, porcReajuste, salNovo;
		
		System.out.print("Digite o salário atual do funcionário: ");
		salAtual = ent.nextDouble();
		System.out.print("Digite o percentual de reajuste: ");
		porcReajuste = ent.nextDouble();
		
		salNovo = salAtual + salAtual * porcReajuste / 100;
		
		System.out.println("O novo salário do funcionário é: R$" + salNovo);
	}
}