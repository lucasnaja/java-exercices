package exercicios;

public class Exercicio03 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		int notas[] = new int[10], menor, maior = 0;
		
		for (int i = 0; i < notas.length; i++) {
			System.out.print("Digite a " + (i + 1) + "ª nota: ");
			notas[i] = ent.nextInt();
			if (notas[i] < 0 || notas[i] > 100)
				i--;
		}
		
		for (int i = 0; i < notas.length; i++)
			for (int j = 0; j < notas.length; j++) 
				if (notas[i] < notas[j]) {
					int temp = notas[i];
					notas[i] = notas[j];
					notas[j] = temp;
				}
		
		menor = notas[0];
		maior = notas[notas.length - 1];
		
		System.out.println("Menor nota: " + menor + '\n' +
												"Maior nota: " + maior);
	}
}