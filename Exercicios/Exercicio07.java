package exercicios;

public class Exercicio07 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		int numeros[] = new int[15], soma = 0;
		
		for (int i = 0; i < numeros.length; i++) {
			int qtdDiv = 0;
			System.out.print("Digite o " + (i + 1) + "° número: ");
			numeros[i] = ent.nextInt();
			for (int j = numeros[i]; j > 0; j--)
				if (numeros[i] % 2 == 0)
					qtdDiv++;
			
			if (qtdDiv == 0)
				soma += numeros[i];
		}
		
		System.out.println("A soma dos números primos é: " + soma);
	}
}