package teste;

import javax.swing.JOptionPane;

public class Exercicio04 {

	public static void main(String[] args) {
		String nome, tipo;
		double credito, debito, saldo, tarifa;
		
		nome = JOptionPane.showInputDialog("Digite o nome do client:");
		credito = Double.parseDouble(JOptionPane.showInputDialog("Digite o cr�dito:"));
		debito = Double.parseDouble(JOptionPane.showInputDialog("Digite o d�bito:"));
		
		if (credito <= 3000) {
			tarifa = 0.15;
			tipo = "Comum";
		}
		else if (credito <= 5000) {
			tarifa = 0.1;
			tipo = "Especial";
		}
		else {
			tarifa = 0.05;
			tipo = "Premium";
		}
		
		saldo = credito - debito;
		
		JOptionPane.showMessageDialog(null, nome + " � um cliente: " + tipo);
		JOptionPane.showMessageDialog(null, "Valor da tarifa: " + saldo * tarifa);
		JOptionPane.showMessageDialog(null, "Saldo atual: " + (saldo - saldo * tarifa));
	}

}
