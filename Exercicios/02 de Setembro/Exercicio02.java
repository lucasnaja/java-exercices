package teste;

import javax.swing.JOptionPane;

public class Exercicio02 {

	public static void main(String[] args) {
		int idade;
		
		idade = Integer.parseInt(JOptionPane.showInputDialog("Digite a idade do atleta:"));
		
		if (idade < 5)
			JOptionPane.showMessageDialog(null, "N�o permitido");
		else if (idade < 8)
			JOptionPane.showMessageDialog(null, "Infantil A");
		else if (idade < 12)
			JOptionPane.showMessageDialog(null, "Infantil B");
		else if (idade < 14)
			JOptionPane.showMessageDialog(null, "Juvenil A");
		else if (idade < 18)
			JOptionPane.showMessageDialog(null, "Juvenil B");
		else JOptionPane.showMessageDialog(null, "Adultos");
	}

}
