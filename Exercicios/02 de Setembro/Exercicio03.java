package teste;

import javax.swing.JOptionPane;

public class Exercicio03 {

	public static void main(String[] args) {
		int idade;
		
		idade = Integer.parseInt(JOptionPane.showInputDialog("Digite a idade:"));
		
		if (idade < 16)
			JOptionPane.showMessageDialog(null, "N�o eleitor");
		else if (idade >= 18 && idade < 65)
			JOptionPane.showMessageDialog(null, "Eleitor obrigat�rio");
		else JOptionPane.showMessageDialog(null, "Eleitor facultativo");
	}

}
