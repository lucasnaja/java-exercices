package teste;

import javax.swing.JOptionPane;

public class Exercicio01 {

	public static void main(String[] args) {
		int[] num = new int[10];
		String cresc = "", decresc = "";
		
		for (int i = 0; i < num.length; i++) {
			num[i] = Integer.parseInt(JOptionPane.showInputDialog("Digite o " + (i + 1) + "� n�mero: "));
		}
			
		for (int i = 0; i < num.length; i++)
			for (int j = i + 1; j < num.length; j++)
				if (num[i] > num[j]) {
					int temp = num[i];
					num[i] = num[j];
					num[j] = temp;
				}
			
		
		for (int i = 1; i <= num.length; i++) {
			cresc += i == num.length ? num[i - 1] + "." : num[i - 1] + ", ";
			decresc += i == num.length ? num[num.length - i] + "." : num[num.length - i] + ", ";
		}
		
		JOptionPane.showMessageDialog(null, "Ordem crescente: " + cresc + '\n' +
											"Ordem decrescente: " + decresc);
	}
}
