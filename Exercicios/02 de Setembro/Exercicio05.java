package teste;

import javax.swing.JOptionPane;

public class Exercicio05 {

	public static void main(String[] args) {
		double altura, peso, IMC;
		
		altura = Double.parseDouble(JOptionPane.showInputDialog("Digite sua altura:"));
		peso = Double.parseDouble(JOptionPane.showInputDialog("Digite seu peso:"));
		
		IMC = peso / (altura * altura);
		
		if (IMC < 19)
			JOptionPane.showMessageDialog(null, "MUITO MAGRO");
		else if (IMC < 25)
			JOptionPane.showMessageDialog(null, "NORMAL");
		else if (IMC < 30)
			JOptionPane.showMessageDialog(null, "SOBREPESO");
		else if (IMC < 40)
			JOptionPane.showMessageDialog(null, "OBESO");
		else JOptionPane.showMessageDialog(null, "OBESIDADE GRAVE");
	}
}
