package exercicios;

import javax.swing.JOptionPane;

public class Exercicio01 {

	public static void main(String[] args) {
		String nome, nome2;
		int ano, ano2;
		
		nome = JOptionPane.showInputDialog("Digite o primeiro nome");
		ano = Integer.parseInt(JOptionPane.showInputDialog("Digite seu ano de nascimento"));

		nome2 = JOptionPane.showInputDialog("Digite o segundo nome");		
		ano2 = Integer.parseInt(JOptionPane.showInputDialog("Digite seu ano de nascimento"));
		
		if (ano < ano2)
			JOptionPane.showMessageDialog(null, "O mais velho se chama " + nome + '\n' +
				"A idade do mais velho � " + (2018 - ano) + '\n' +
				"A diferen�a de idades � " + (ano2 - ano));
		 else if (ano2 < ano)
			JOptionPane.showMessageDialog(null, "O mais velho se chama " + nome2 + '\n' +
					"A idade do mais velho � " + (2018 - ano2) + '\n' +
					"A diferen�a de idades � " + (ano - ano2));
		 else 
			JOptionPane.showMessageDialog(null, "Os dois possuem a mesma idade!");
	}
}
