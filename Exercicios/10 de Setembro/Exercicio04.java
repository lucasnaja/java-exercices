package Atividade;

import javax.swing.JOptionPane;

public class ex4 {
	public static void main(String [] args) {
		String tipo;
		double alcool, gasolina, desconto;
		
		tipo = JOptionPane.showInputDialog("Escreva o tipo de gasolina [A/G]:");
		
		if (tipo.equalsIgnoreCase("A")) {
			alcool = Double.parseDouble(JOptionPane.showInputDialog("Digite a quantidade de �lcool:"));
			
			if (alcool <= 20) 
				desconto = 0.05;
			else 
				desconto = 0.1;
			
			alcool *= 2.79;
			JOptionPane.showMessageDialog(null, "Pre�o final: " + (alcool - alcool * desconto));
		} else if (tipo.equalsIgnoreCase("G")) {
			gasolina = Double.parseDouble(JOptionPane.showInputDialog("Digite a quantidade de gasolina:"));
			
			if (gasolina <= 20) 
				desconto = 0.05;
			else 
				desconto = 0.1;
			
			gasolina *= 4.1;
			JOptionPane.showMessageDialog(null, "Pre�o final: " + (gasolina - gasolina * desconto));
		} else {
			JOptionPane.showMessageDialog(null, "Inv�lido");
		}
	}
}