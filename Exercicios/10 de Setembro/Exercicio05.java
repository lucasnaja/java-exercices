package exercicios;

import javax.swing.JOptionPane;

public class Exercicio05 {

	public static void main(String[] args) {
		String sexo;
		
		sexo = JOptionPane.showInputDialog("Digite seu sexo [M/F]");
		
		if (sexo.equalsIgnoreCase("M"))
			JOptionPane.showMessageDialog(null, "Masculino");
		else if (sexo.equalsIgnoreCase("F"))
			JOptionPane.showMessageDialog(null, "Feminino");
		else JOptionPane.showMessageDialog(null, "Inv�lido");
	}
}
