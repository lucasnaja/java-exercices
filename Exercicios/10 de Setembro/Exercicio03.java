package exercicios;

import javax.swing.JOptionPane;

public class Exercicio03 {

	public static void main(String[] args) {
		double valor, subTotal, desconto;
		int qtd;
		
		valor = Double.parseDouble(JOptionPane.showInputDialog("Digite o valor do produto"));
		qtd = Integer.parseInt(JOptionPane.showInputDialog("Digite a quantidade de produtos vendidos"));
		
		subTotal = valor * qtd;
		
		if (subTotal <= 1000)
			desconto = 0.1 * subTotal;
		else if (subTotal <= 3000)
			desconto = 0.2 * subTotal;
		else if (subTotal <= 5000)
			desconto = 0.3 * subTotal;
		else desconto = 0.5 * subTotal;
		
		JOptionPane.showMessageDialog(null, "O valor total a pagar � " + (subTotal - desconto));
	}
}
