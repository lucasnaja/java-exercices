package exercicios;

import javax.swing.JOptionPane;

public class Exercicio02 {

	public static void main(String[] args) {
		double kmI, kmF, kmMedia, pUni, valorMercadoria, IPI, frete, valorFinal;
		int qtd;
		
		kmI = Double.parseDouble(JOptionPane.showInputDialog("Digite os KM iniciais"));
		kmF = Double.parseDouble(JOptionPane.showInputDialog("Digite os KM finais"));
		qtd = Integer.parseInt(JOptionPane.showInputDialog("Digite a quantidade de mercadorias vendidas"));
		pUni = Double.parseDouble(JOptionPane.showInputDialog("Digite o pre�o unit�rio"));
		
		valorMercadoria = qtd * pUni;
		IPI = 0.02 * valorMercadoria;
		kmMedia = kmF - kmI;
		
		if (kmMedia <= 500)
			frete = 0.10 * valorMercadoria;
		else if (kmMedia <= 1000)
			frete = 0.15 * valorMercadoria;
		else if (kmMedia <= 1500)
			frete = 0.20 * valorMercadoria;
		else 
			frete = 0.25 * valorMercadoria;
		
		valorFinal = valorMercadoria + IPI + frete;
		
		JOptionPane.showMessageDialog(null, "O valor final �: " + valorFinal);
	}
}
