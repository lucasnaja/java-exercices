package exercicios;

public class Exercicio06 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		int[] numeros = new int[3];
		
		for (int i = 0; i < numeros.length; i++) {
			System.out.print("Digite o " + (i + 1) + "° número: ");
			numeros[i] = ent.nextInt();
		}
		
		for (int i = 0; i < numeros.length; i++)
			for (int j = 0; j < numeros.length; j++) 
				if (numeros[i] < numeros[j]) {
					int temp = numeros[i];
					numeros[i] = numeros[j];
					numeros[j] = temp;
				}
		
		for (int i = 0; i < numeros.length; i++)
			System.out.print(numeros[i] + (i == numeros.length - 1 ? "" : ", "));
	}
}