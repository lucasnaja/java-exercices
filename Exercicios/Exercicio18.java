package exercicios;

public class Exercicio18 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		String sexo;
		double altura;
		
		System.out.print("Digite o sexo da pessoa [M/F]: ");
		sexo = ent.nextLine();
		System.out.print("Digite a altura da pessoa [Ex: 1.71]: ");
		altura = ent.nextDouble();
		
		if (sexo.equalsIgnoreCase("M"))
			System.out.println("Peso ideal: " + (72.7 * altura - 58));
		else System.out.println("Peso ideal: " + (62.1 * altura - 44.7));
	}
}