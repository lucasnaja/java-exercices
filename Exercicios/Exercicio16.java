package exercicios;

public class Exercicio16 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		double nota1, nota2, media;
		
		System.out.print("Digite a primeira nota: ");
		nota1 = ent.nextDouble();
		System.out.print("Digite a segunda nota: ");
		nota2 = ent.nextDouble();
		
		media = (nota1 + nota2) / 2;
		if (media >= 6)
			System.out.println("O aluno foi aprovado! Média: " + media);
		else System.out.println("O aluno foi reprovado! Média: " + media);
	}
}