package exercicios;

public class Exercicio14 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		int valor;
		
		System.out.print("Digite o valor: ");
		valor = ent.nextInt();
		
		if (valor < 0)
			System.out.println("NEGATIVO");
		else System.out.println("POSITIVO");
	}
}