package exercicios;

public class Exercicio12 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		double tempF, tempC;
		
		System.out.print("Digite a temperatura em °F: ");
		tempF = ent.nextDouble();
		
		tempC = ((tempF - 32) / 9) * 5;
		System.out.println("O valor em °C é: " + tempC + "°C");
	}
}