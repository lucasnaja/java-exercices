package exercicios;

public class Exercicio02 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		int x, n, result;
		
		System.out.print("Digite o valor de X: ");
		x = ent.nextInt();
		System.out.print("Digite o valor de N: ");
		n = ent.nextInt();
		n = n < 0 ? -n : n;
		
		result = (int)Math.pow(x, n);
		
		System.out.println(x + " elevado a " + n + " = " + result);
	}
}