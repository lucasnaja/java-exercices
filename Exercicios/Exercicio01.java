package exercicios;
public class Exercicio01 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		int n;
		
		System.out.print("Digite um número: ");
		n = ent.nextInt();
		
		for (int i = 0; i <= n; i++)
			if (i % 2 == 1)
				System.out.print(i + (n % 2 == 1 ? (i == n ? "" : ", ") : (i == n - 1 ? "" : ", ")));
	}
}