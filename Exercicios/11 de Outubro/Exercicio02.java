package exercicio;

public class Exercicio02 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		int num;
		
		System.out.print("Digite o n�mero: ");
		num = ent.nextInt();
		
		System.out.println("Tabuada do " + num);
		System.out.println("============");
		for (int i = 10; i > 0; i--)
			System.out.println(num + " x " + i + " = " + num * i);
	}
}