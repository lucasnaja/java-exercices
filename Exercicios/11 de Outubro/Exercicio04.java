package exercicio;

import javax.swing.JOptionPane;

public class Exercicio04 {

	public static void main(String[] args) {
		String listaMaior = "", listaMenor = "";
		int qtdMaior = 0, qtdMenor = 0; 
		for (int i = 0; i < 20; i++) {
			int num = (int)(Math.random() * 100);
			if (num < 50) {
				qtdMenor++;
				listaMenor += num + "-";
			} else {
				qtdMaior++;
				listaMaior += num + "-";
			}
		}
		
		JOptionPane.showMessageDialog(null, "Maiores que 50: " + qtdMaior + '\n' +
				listaMaior + '\n' +
				"Menores ou iguals a 50: " + qtdMenor + '\n' +
				listaMenor);
	}
}