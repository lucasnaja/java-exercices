package exercicio;

public class Exercicio03 {

	public static void main(String[] args) {
		for (int i = 30; i > 0; i--)
			if (i % 2 == 0)
				System.out.print(i + "|");
		
		System.out.println();
		
		for (int i = 30; i > 0; i--)
			if (i % 2 == 1)
				System.out.print(i + "|");
	}
}