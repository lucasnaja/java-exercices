package exercicios;

public class Exercicio13 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		double valor;
		
		System.out.print("Digite o valor: ");
		valor = ent.nextDouble();
		
		if (valor < 10)
			System.out.println("MENOR QUE 10!");
		else System.out.println("MAIOR QUE 10!");
	}
}