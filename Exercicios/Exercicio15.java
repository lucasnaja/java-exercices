package exercicios;

public class Exercicio15 {

	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		int qtdMacas;
		double preco;
		
		System.out.print("Digite a quantidade de maçãs compradas: ");
		qtdMacas = ent.nextInt();
		
		if (qtdMacas < 12)
			preco = qtdMacas * 1.30;
		else preco = qtdMacas;
		
		System.out.println("O preço das maçãs ficou: R$" + preco);
	}
}