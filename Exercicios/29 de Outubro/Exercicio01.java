package exercicio;

public class Exercicio {
	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		System.out.print("Digite um n�mero em CM: ");
		int num = ent.nextInt(), km, m;
		ent.close();
		
		km = num / 100000;
		num -= km * 100000;
		
		m = num / 100;
		num -= m * 100;
		
		System.out.print(km + "km " + m + "m " + num + "cm");
	}
}
