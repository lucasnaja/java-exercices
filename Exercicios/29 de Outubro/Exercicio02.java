package exercicio;

public class Exercicio {
	public static void main(String[] args) {
		java.util.Scanner ent = new java.util.Scanner(System.in);
		System.out.print("Digite um n�mero em CM: ");
		int num = ent.nextInt(), cem, cin, dez;
		ent.close();

		cem = num / 100;
		num -= cem * 100;

		cin = num / 50;
		num -= cin * 50;

		dez = num / 10;
		num -= dez * 10;

		System.out.print((cem == 0 ? "" : cem + " notas de R$100,00\n") +
				(cin == 0 ? "" : cin + " notas de R$50,00\n") +
				(dez == 0 ? "" : dez + " notas de R$10,00\n") +
				(num == 0 ? "" : num + " notas de R$1,00"));
	}
}
