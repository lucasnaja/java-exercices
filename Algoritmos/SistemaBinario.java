package binario;

public class SistemaBinario {
	public void ConverterParaTexto(String binary) {
		String[] binarys = binary.split(" ");
		System.out.print("Texto em AscII: ");
		for (int i = 0; i < binarys.length; i++) {
			int qtd = 0, atual = 1;
			for (int j = binarys[i].length(); j > 0; j--) {
				qtd += Integer.parseInt(binarys[i].substring(j - 1, j)) == 0 ? 0 : atual;
				atual *= 2;
			}
			System.out.print((char)qtd);
			if (binarys.length == 1)
				System.out.print(", Código decimal: " + qtd);
		}
		System.out.println();
	}
	
	private String Reverter(long number) {
		String binary = "", result = "";
		while (number != 0) {
			binary += number % 2;
			number /= 2;
		}
		
		int length = binary.length();
		if (binary.length() < 8)
			for (int i = 0; i < (8 - length); i++)
					binary += '0';
		
		for (int i = binary.length(); i > 0; i--)
			result += binary.substring(i - 1, i);
		
		return result;
	}
	
	public void ConverterParaBinario(char letter) {
		System.out.println("Código binário: " + Reverter((int)letter) + ", código decimal: " + (int)letter);	
	}
	
	public void ConverterParaBinario(int number) {
		System.out.println("Código binário: " + Reverter(number) + ", código AscII: " + (char)number);	
	}
	
	public void ConverterParaBinario(String text) {
		if (text.length() > 0)
			System.out.print("Código binário: ");
		if (text.length() == 1)
			System.out.println(Reverter((int)text.charAt(0)) + ", código decimal: " + (int)text.charAt(0));
		else
			for (int i = 0; i < text.length(); i++)
				System.out.print(Reverter((int)text.charAt(i)) + (i == text.length() - 1 ? '\n' : ' '));	
	}
	
	public void ConverterParaBinario() {
		java.util.Scanner reader = new java.util.Scanner(System.in);
		String text;
		System.out.print("Digite o texto: ");
		text = reader.nextLine();
		ConverterParaBinario(text);
	}
	
	public void ConverterParaTexto() {
		java.util.Scanner reader = new java.util.Scanner(System.in);
		String binaryCode;
		System.out.print("Digite o código binário: ");
		binaryCode = reader.nextLine();
		ConverterParaTexto(binaryCode);
	}
}
